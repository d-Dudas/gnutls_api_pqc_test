/**
 * This program should create a certificate
 * using dilithium key and certificate request
 * with dilithium key, signed with SHAKE_256.
 * The certificate should be exported to file CRT_SELF_SIGNED_FILE
 **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "self_signed_crt.h"

int sscrt_set_key_and_crq(gnutls_x509_crt_t crt, gnutls_x509_privkey_t pkey)
{
    int ret;
    gnutls_x509_crq_t crq;
    gnutls_datum_t data;

    CHECK(gnutls_x509_crq_init(&crq));

    CHECK(gnutls_load_file(PKCS1_FILE, &data));
    CHECK(gnutls_x509_privkey_import(pkey, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_load_file(CRQ_FILE, &data));
    CHECK(gnutls_x509_crq_import(crq, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_x509_crt_set_key(crt, pkey));
    CHECK(gnutls_x509_crt_set_crq(crt, crq));

    gnutls_x509_crq_deinit(crq);

    return SUCCESS;

error:
    return ret;
}

int sscrt_set_tbs_params(gnutls_x509_crt_t crt)
{
    int ret;

    CHECK(gnutls_x509_crt_set_activation_time(crt, time(NULL)));
    CHECK(gnutls_x509_crt_set_expiration_time(crt, time(NULL) + 7*24*60*60)) // 7 days

    CHECK(gnutls_x509_crt_set_key_usage(crt, GNUTLS_KEY_DIGITAL_SIGNATURE));

    CHECK(gnutls_x509_crt_set_serial(crt, CERT_SERIAL, strlen(CERT_SERIAL)));

    CHECK(gnutls_x509_crt_set_version(crt, 3));

    CHECK(gnutls_x509_crt_set_ca_status(crt, 1));

    CHECK(gnutls_x509_crt_set_key_purpose_oid(crt, GNUTLS_KP_TLS_WWW_CLIENT, 0));

    CHECK(gnutls_x509_crt_set_subject_key_id(crt, SELF_SIGNED_CRT_SUBJECT_ID, strlen(SELF_SIGNED_CRT_SUBJECT_ID)));
    CHECK(gnutls_x509_crt_set_subject_alternative_name(crt, GNUTLS_SAN_DNSNAME, "localhost"))

    return SUCCESS;

error:
    return ret;
}

int test_self_signed_crt()
{
    int ret;
    gnutls_x509_crt_t crt;
    gnutls_x509_privkey_t pkey;
    gnutls_datum_t data;
    gnutls_digest_algorithm_t digest;

    CHECK(gnutls_global_init());
    CHECK(gnutls_x509_privkey_init(&pkey));
    CHECK(gnutls_x509_crt_init(&crt));

    CHECK(sscrt_set_key_and_crq(crt, pkey));
    CHECK(sscrt_set_tbs_params(crt));

    digest = get_pk_digest(gnutls_x509_privkey_get_pk_algorithm(pkey));

    CHECK(gnutls_x509_crt_sign2(crt, crt, pkey, digest, 0));

    CHECK(gnutls_x509_crt_export2(crt, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(CRT_SELF_SIGNED_FILE, data.data, data.size);

    gnutls_x509_crt_deinit(crt);
    gnutls_x509_privkey_deinit(pkey);
    gnutls_global_deinit();

    return SUCCESS;
error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ERROR;
}
