#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <gnutls/gnutls.h>

#include "common.h"

int save_to_file(char* filename, unsigned char* uBuffer, size_t buffer_size)
{
    char* buffer = malloc(buffer_size * sizeof(char));
    memcpy(buffer, uBuffer, buffer_size);
    return save_to_file2(filename, buffer, buffer_size);
}

int save_to_file2(char* filename, char* buffer, size_t buffer_size)
{
	FILE* output_file = fopen(filename, "wb");
	if(output_file == NULL) return ERROR;

	size_t elements_written = fwrite(buffer, buffer_size, 1, output_file);

	fclose(output_file);

	if(elements_written != buffer_size) return ERROR;

	return SUCCESS;
}

int cmp_files(char* first_file, char* second_file) {
    FILE *file1, *file2;
    int ret = 0;

    file1 = fopen(first_file, "rb");
    file2 = fopen(second_file, "rb");

    if (file1 == NULL || file2 == NULL) {
        perror("Error opening files");
        if (file1 != NULL) fclose(file1);
        if (file2 != NULL) fclose(file2);
        return ERROR;
    }

    int ch1, ch2;
    while ((ch1 = fgetc(file1)) != EOF && (ch2 = fgetc(file2)) != EOF) {
        if (ch1 != ch2) {
            ret = ch1 - ch2;
            break;
        }
    }

    fclose(file1);
    fclose(file2);

    return ret;
}

gnutls_digest_algorithm_t get_pk_digest(gnutls_pk_algorithm_t algo)
{
    switch (algo) 
    {
        case GNUTLS_PK_ML_DSA_44:
        case GNUTLS_PK_ML_DSA_65:
        case GNUTLS_PK_ML_DSA_87:
        default:
            return GNUTLS_DIG_SHAKE_256;
        // case GNUTLS_PK_FALCON_512:
        // case GNUTLS_PK_FALCON_1024:
        // default:
        //     return GNUTLS_DIG_SHAKE_256;
    }
}
