/**
 * This program should create a certificate
 * using dilithium key and certificate request
 * with dilithium key, signed with SHAKE_256.
 * The certificate should be exported to file CRT_FILE
 **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "crt.h"

int crt_set_key_and_crq(gnutls_x509_crt_t crt, gnutls_x509_privkey_t pkey)
{
    int ret;
    gnutls_x509_crq_t crq;
    gnutls_datum_t data;

    CHECK(gnutls_x509_crq_init(&crq));

    CHECK(gnutls_load_file(PKCS1_FILE, &data));
    CHECK(gnutls_x509_privkey_import(pkey, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_load_file(CRQ_FILE, &data));
    CHECK(gnutls_x509_crq_import(crq, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_x509_crt_set_key(crt, pkey));
    CHECK(gnutls_x509_crt_set_crq(crt, crq));

    gnutls_x509_crq_deinit(crq);

    return SUCCESS;

error:
    return ret;
}

int crt_set_tbs_params(gnutls_x509_crt_t crt)
{
    int ret;

    CHECK(gnutls_x509_crt_set_activation_time(crt, time(NULL)));
    CHECK(gnutls_x509_crt_set_expiration_time(crt, time(NULL) + 7*24*60*60)) // 7 days

    // CHECK(gnutls_x509_crt_set_dn_by_oid(crt, GNUTLS_OID_X520_COUNTRY_NAME, 0,
    //                                     CERT_COUNTRY_NAME, strlen(CERT_COUNTRY_NAME)));
    // CHECK(gnutls_x509_crt_set_dn_by_oid(crt, GNUTLS_OID_X520_ORGANIZATION_NAME, 0,
    //                                     CERT_ORG_NAME, strlen(CERT_ORG_NAME)));
    // CHECK(gnutls_x509_crt_set_dn_by_oid(crt, GNUTLS_OID_X520_COMMON_NAME, 0,
    //                                     CERT_SUBJECT_NAME, strlen(CERT_SUBJECT_NAME)));

    // CHECK(gnutls_x509_crt_set_issuer_dn_by_oid(crt, GNUTLS_OID_X520_COUNTRY_NAME, 0,
    //                                            CERT_COUNTRY_NAME, strlen(CERT_COUNTRY_NAME)));
    // CHECK(gnutls_x509_crt_set_issuer_dn_by_oid(crt, GNUTLS_OID_X520_ORGANIZATION_NAME, 0,
    //                                            CERT_ORG_NAME, strlen(CERT_ORG_NAME)));
    // CHECK(gnutls_x509_crt_set_issuer_dn_by_oid(crt, GNUTLS_OID_X520_ORGANIZATIONAL_UNIT_NAME, 0,
    //                                            CERT_ORG_UNIT_NAME, strlen(CERT_ORG_UNIT_NAME)));
    // CHECK(gnutls_x509_crt_set_issuer_dn_by_oid(crt, GNUTLS_OID_X520_COMMON_NAME, 0,
    //                                            CERT_ISSUER_NAME, strlen(CERT_ISSUER_NAME)));

    CHECK(gnutls_x509_crt_set_key_usage(crt, GNUTLS_KEY_DIGITAL_SIGNATURE | GNUTLS_KEY_KEY_CERT_SIGN));
    gnutls_x509_crt_set_basic_constraints(crt, 1, -1);
    // CHECK(gnutls_x509_crt_set_key_usage(crt, GNUTLS_KEY_KEY_ENCIPHERMENT));
    // CHECK(gnutls_x509_crt_set_key_usage(crt, GNUTLS_KEY_DATA_ENCIPHERMENT));

    CHECK(gnutls_x509_crt_set_serial(crt, CERT_SERIAL, strlen(CERT_SERIAL)));

    CHECK(gnutls_x509_crt_set_version(crt, 3));

    CHECK(gnutls_x509_crt_set_ca_status(crt, 0));

    CHECK(gnutls_x509_crt_set_key_purpose_oid(crt, GNUTLS_KP_TLS_WWW_CLIENT, 0));

    CHECK(gnutls_x509_crt_set_subject_key_id(crt, SUBJECT_ID, strlen(SUBJECT_ID)));
    CHECK(gnutls_x509_crt_set_subject_alternative_name(crt, GNUTLS_SAN_DNSNAME, "localhost"))
    CHECK(gnutls_x509_crt_set_authority_key_id(crt, SELF_SIGNED_CRT_SUBJECT_ID, strlen(SELF_SIGNED_CRT_SUBJECT_ID)));

    return SUCCESS;

error:
    return ret;
}

int import_self_signed_crt(gnutls_x509_crt_t self_signed_crt)
{
    int ret;
    gnutls_datum_t data;

    CHECK(gnutls_load_file(CRT_SELF_SIGNED_FILE, &data));
    CHECK(gnutls_x509_crt_import(self_signed_crt, &data, GNUTLS_X509_FMT_PEM));

    return SUCCESS;
error:
    return ret;
}

int test_crt()
{
    int ret;
    gnutls_x509_crt_t crt;
    gnutls_x509_crt_t self_signed_crt;
    gnutls_x509_privkey_t pkey;
    gnutls_datum_t data;
    gnutls_digest_algorithm_t digest;

    CHECK(gnutls_global_init());
    CHECK(gnutls_x509_privkey_init(&pkey));
    CHECK(gnutls_x509_crt_init(&crt));
    CHECK(gnutls_x509_crt_init(&self_signed_crt));

    CHECK(import_self_signed_crt(self_signed_crt));

    CHECK(crt_set_key_and_crq(crt, pkey));
    CHECK(crt_set_tbs_params(crt));

    digest = get_pk_digest(gnutls_x509_privkey_get_pk_algorithm(pkey));

    CHECK(gnutls_x509_crt_sign2(crt, self_signed_crt, pkey, digest, 0));

    CHECK(gnutls_x509_crt_export2(crt, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(CRT_FILE, data.data, data.size);

    gnutls_x509_crt_deinit(self_signed_crt);
    gnutls_x509_crt_deinit(crt);
    gnutls_x509_privkey_deinit(pkey);
    gnutls_global_deinit();

    return SUCCESS;
error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ERROR;
}
