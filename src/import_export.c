#include <stdio.h>

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#include "common.h"
#include "import_export.h"

int verify_pkcs1_import_export()
{
    int ret;
    gnutls_datum_t data;
    gnutls_x509_privkey_t private_key2;

    CHECK(gnutls_x509_privkey_init(&private_key2));

    CHECK(gnutls_load_file(PKCS1_FILE, &data));
    CHECK(gnutls_x509_privkey_import2(private_key2, &data, GNUTLS_X509_FMT_PEM, NULL, 0));

    CHECK(gnutls_x509_privkey_export2(private_key2, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(PKCS1_FILE2, data.data, data.size);

    gnutls_x509_privkey_deinit(private_key2);

    if(cmp_files(PKCS1_FILE, PKCS1_FILE2) != 0)
    {
        printf("Key pkcs1 import/export printfed.\n");
        return TEST_FAILED;
    }

    return SUCCESS;

error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ret;
}

int verify_pkcs8_import_export()
{
    int ret;
    gnutls_datum_t data;
    gnutls_x509_privkey_t private_key2;

    CHECK(gnutls_x509_privkey_init(&private_key2));

    CHECK(gnutls_load_file(PKCS8_FILE, &data));
    CHECK(gnutls_x509_privkey_import_pkcs8(private_key2, &data, GNUTLS_X509_FMT_PEM, NULL, 0));

    CHECK(gnutls_x509_privkey_export2_pkcs8(private_key2, GNUTLS_X509_FMT_PEM, NULL, 0, &data));
    save_to_file(PKCS8_FILE2, data.data, data.size);

    gnutls_x509_privkey_deinit(private_key2);

    if(cmp_files(PKCS8_FILE, PKCS8_FILE2) < 0)
    {
        printf("Key pkcs8 import/export printfed.\n");
        return TEST_FAILED;
    }

    return SUCCESS;

error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ret;
}

int test_key_import_export()
{
    int ret;

    TEST(verify_pkcs1_import_export());
    TEST(verify_pkcs8_import_export());

    return SUCCESS;

test_failed:
    return TEST_FAILED;
}

int verify_sscrt_import_export()
{
    int ret;
    gnutls_x509_crt_t crt2;
    gnutls_datum_t data;

    CHECK(gnutls_x509_crt_init(&crt2));

    CHECK(gnutls_load_file(CRT_SELF_SIGNED_FILE, &data));
    CHECK(gnutls_x509_crt_import(crt2, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_x509_crt_export2(crt2, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(CRT_SELF_SIGNED_FILE2, data.data, data.size);

    gnutls_x509_crt_deinit(crt2);

    if(cmp_files(CRT_SELF_SIGNED_FILE, CRT_SELF_SIGNED_FILE2) != 0)
    {
        printf("SS Certificate import/export failed.\n");
        return TEST_FAILED;
    }

    return SUCCESS;

error:
    return ret;
}

int test_sscrt_import_export()
{
    int ret;

    TEST(verify_sscrt_import_export());

    return SUCCESS;

test_failed:
    return TEST_FAILED;
}

int verify_crq_import_export()
{
    int ret;
    gnutls_x509_crq_t crq2;
    gnutls_datum_t data;

    CHECK(gnutls_x509_crq_init(&crq2));

    CHECK(gnutls_load_file(CRQ_FILE, &data));
    CHECK(gnutls_x509_crq_import(crq2, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_x509_crq_export2(crq2, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(CRQ_FILE2, data.data, data.size);

    gnutls_x509_crq_deinit(crq2);

    if(cmp_files(CRQ_FILE, CRQ_FILE2) != 0)
    {
        printf("Crq import/export failed.\n");
        return TEST_FAILED;
    }

    return SUCCESS;

error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ret;
}

int test_crq_import_export()
{
    int ret;

    TEST(verify_crq_import_export());

    return SUCCESS;

test_failed:
    return TEST_FAILED;
}

int verify_crt_import_export()
{
    int ret;
    gnutls_x509_crt_t crt2;
    gnutls_datum_t data;

    CHECK(gnutls_x509_crt_init(&crt2));

    CHECK(gnutls_load_file(CRT_FILE, &data));
    CHECK(gnutls_x509_crt_import(crt2, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_x509_crt_export2(crt2, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(CRT_FILE2, data.data, data.size);

    gnutls_x509_crt_deinit(crt2);

    if(cmp_files(CRT_FILE, CRT_FILE2) != 0)
    {
        printf("Certificate import/export failed.\n");
        return TEST_FAILED;
    }

    return SUCCESS;

error:
    return ret;
}

int test_crt_import_export()
{
    int ret;

    TEST(verify_crt_import_export());

    return SUCCESS;

test_failed:
    return TEST_FAILED;
}
