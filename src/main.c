#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <gnutls/gnutls.h>

#include "common.h"
#include "crl.h"
#include "import_export.h"
#include "key_generation.h"
#include "sign.h"
#include "crq.h"
#include "self_signed_crt.h"
#include "crt.h"

typedef struct {
    gnutls_pk_algorithm_t algo;
    char* name;
} algorithm;

algorithm algorithms[] = {
    {GNUTLS_PK_ML_DSA_44, "ML_DSA_44"},
    {GNUTLS_PK_ML_DSA_65, "ML_DSA_65"},
    {GNUTLS_PK_ML_DSA_87, "ML_DSA_87"},
    // {GNUTLS_PK_FALCON_512, "FALCON_512"},
    // {GNUTLS_PK_FALCON_1024, "FALCON_1024"},j

    {0, NULL}
};

void print_alg_name(char* name)
{
    int dots_required = 40;
    int dots_remained = dots_required - strlen(name);
    printf("%s", name);
    for(int i = 0; i < dots_remained; i++) printf(".");
}

int test_algorithm(gnutls_pk_algorithm_t algo)
{
    int ret;
        TEST_MSG(test_key_generation(algo), "Key generation");
        TEST_MSG(test_key_import_export(), "Key import/export");
        TEST_MSG(test_sign_data(algo), "Sign data");
        TEST_MSG(test_create_crq(), "Creating crq");
        TEST_MSG(test_crq_import_export(), "CRQ import export");
        TEST_MSG(test_self_signed_crt(), "Self signed crt");
        TEST_MSG(test_sscrt_import_export(), "SS Crt import export");
        TEST_MSG(test_crt(), "Creating CRT");
        TEST_MSG(test_crt_import_export(), "CRT import export");
        TEST_MSG(test_create_crl(), "Creating CRL");
    
    return SUCCESS;

test_failed:
    return TEST_FAILED;
}


void test_all_algorithms()
{
    int ret;

    for(algorithm *algo = algorithms; algo->name != NULL; algo++)
    {
        print_alg_name(algo->name);

        ret = test_algorithm(algo->algo);

        printf(ret == SUCCESS ? "PASSED\n" : "\n\n");
    }
}

int main(int argc,char **argv)
{
    // test_all_algorithms();
    test_algorithm(GNUTLS_PK_ML_DSA_44);

    return SUCCESS;
}
