/*
 *  This program should generate a dilithium keypair
 *  save them to a file and read them to a different
 *  variable.
 **/

#include <stdio.h>
#include <stdlib.h>

#include <gnutls/abstract.h>

#include "common.h"
#include "key_generation.h"

int test_key_generation(gnutls_pk_algorithm_t algo)
{
    int ret;
    gnutls_x509_privkey_t private_key;
    gnutls_datum_t data;
    unsigned int bits;

    CHECK(gnutls_global_init());
    CHECK(gnutls_x509_privkey_init(&private_key));

    bits = gnutls_sec_param_to_pk_bits(algo,
					   GNUTLS_SEC_PARAM_LOW);
    CHECK(gnutls_x509_privkey_generate(private_key, algo, bits, 0));

    data.data = malloc(bits * sizeof(char));
    data.size = bits;

    CHECK(gnutls_x509_privkey_export2(private_key, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(PKCS1_FILE, data.data, data.size);

    CHECK(gnutls_x509_privkey_export2_pkcs8(private_key, GNUTLS_X509_FMT_PEM, NULL, 0, &data));
    save_to_file(PKCS8_FILE, data.data, data.size);

    gnutls_x509_privkey_deinit(private_key);
    gnutls_global_deinit();

    return SUCCESS;

error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ERROR;
}
