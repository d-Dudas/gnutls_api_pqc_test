#include "crl.h"
#include "common.h"
#include "gnutls/gnutls.h"
#include "gnutls/x509.h"
#include <stdio.h>
#include <string.h>

int test_create_crl()
{
    gnutls_x509_crl_t crl;
    int ret;

    ret = gnutls_x509_crl_init(&crl);
    if (ret < 0) {
        printf("Error: %s\n", gnutls_strerror(ret));
        return ERROR;
    }

    gnutls_x509_crt_t ca_cert;
    gnutls_x509_privkey_t ca_key;
    gnutls_datum_t data;

    CHECK(gnutls_x509_crt_init(&ca_cert));
    CHECK(gnutls_x509_privkey_init(&ca_key));

    CHECK(gnutls_load_file(PKCS1_FILE, &data));
    CHECK(gnutls_x509_privkey_import(ca_key, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_load_file(CRT_FILE, &data));
    CHECK(gnutls_x509_crt_import(ca_cert, &data, GNUTLS_X509_FMT_PEM));

    // gnutls_x509_crl_set_crt_issuer(crl, ca_cert);
    time_t now = time(NULL);
    time_t expire = now + (365 * 24 * 60 * 60); // 1 year validity

    CHECK(gnutls_x509_crl_set_crt(crl, ca_cert, expire));
    CHECK(gnutls_x509_crl_set_this_update(crl, now));
    CHECK(gnutls_x509_crl_set_next_update(crl, expire));

    const char *serial_hex = "0123456789ABCDEF"; // Example serial number
    CHECK(gnutls_x509_crl_set_crt_serial(crl, serial_hex, strlen(serial_hex) / 2, expire));
    CHECK(gnutls_x509_crl_set_version(crl, 1));

    // unsigned int reason = GNUTLS_CRL_REASON_KEY_COMPROMISE;

    // gnutls_datum_t serial = { (void *)serial_hex, strlen(serial_hex) / 2 };

    // ret = gnutls_x509_crl_add(crl, &serial, now, reason);
    // if (ret < 0) {
    //     printf("Error adding revoked certificate: %s\n", gnutls_strerror(ret));
    // }

    CHECK(gnutls_x509_crl_sign2(crl, ca_cert, ca_key, GNUTLS_DIG_SHAKE_256, 0));

    CHECK(gnutls_x509_crl_export2(crl, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(CRL_FILE, data.data, data.size);

    gnutls_x509_crl_deinit(crl);
    gnutls_x509_crt_deinit(ca_cert);
    gnutls_x509_privkey_deinit(ca_key);

    gnutls_global_deinit();

    return SUCCESS;
error:
    printf("Error: %s\n", gnutls_strerror(ret));
    return ERROR;
}
