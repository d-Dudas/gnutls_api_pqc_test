#include <stdio.h>

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#include "common.h"

int use_sha(gnutls_pk_algorithm_t algo)
{
    switch (algo)
    {
        case GNUTLS_PK_ML_DSA_44:
        case GNUTLS_PK_ML_DSA_65:
        case GNUTLS_PK_ML_DSA_87:
        default:    
            return 1;
        // case GNUTLS_PK_FALCON_512:
        // case GNUTLS_PK_FALCON_1024:
        // default:
        //     return 0;
    }
}

int test_create_crq()
{
    int ret;
    gnutls_x509_crq_t crq;
	gnutls_x509_privkey_t pkey;
    gnutls_digest_algorithm_t digest;
	gnutls_datum_t data;

    CHECK(gnutls_global_init());
    CHECK(gnutls_x509_crq_init(&crq));
    CHECK(gnutls_x509_privkey_init(&pkey));

    CHECK(gnutls_load_file(PKCS1_FILE, &data));
    CHECK(gnutls_x509_privkey_import(pkey, &data, GNUTLS_X509_FMT_PEM));

    CHECK(gnutls_x509_crq_set_dn_by_oid(crq, GNUTLS_OID_X520_COUNTRY_NAME, 0, "RO", 2));
	// CHECK(gnutls_x509_crq_set_dn_by_oid(crq, GNUTLS_OID_X520_COMMON_NAME, 0, "Nikos", strlen("Nikos")));

    CHECK(gnutls_x509_crq_set_version(crq, 1));

    CHECK(gnutls_x509_crq_set_challenge_password(crq, "something to remember here"));

    CHECK(gnutls_x509_crq_set_key(crq, pkey));

    digest = GNUTLS_DIG_SHAKE_256;

    CHECK(gnutls_x509_crq_sign2(crq, pkey, digest, 0));

    CHECK(gnutls_x509_crq_export2(crq, GNUTLS_X509_FMT_PEM, &data));
    save_to_file(CRQ_FILE, data.data, data.size);

    gnutls_x509_privkey_deinit(pkey);
    gnutls_x509_crq_deinit(crq);
    gnutls_global_deinit();

    return SUCCESS;
error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ERROR;
}
