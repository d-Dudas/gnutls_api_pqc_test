#include <stdio.h>

#include <gnutls/abstract.h>

#include "common.h"
#include "sign.h"

int test_sign_data(gnutls_pk_algorithm_t algorithm)
{
	int ret;
	gnutls_privkey_t privkey;
	gnutls_x509_privkey_t private_key;
	gnutls_pubkey_t pubkey;
	gnutls_datum_t signature;
	gnutls_datum_t data;
	gnutls_digest_algorithm_t digest;
	unsigned vflags = 0;

    gnutls_datum_t raw_data = { (void *)"hello there", 11 };

	CHECK(gnutls_privkey_init(&privkey));
	CHECK(gnutls_x509_privkey_init(&private_key));

	CHECK(gnutls_load_file(PKCS1_FILE, &data));
    CHECK(gnutls_x509_privkey_import2(private_key, &data, GNUTLS_X509_FMT_PEM, NULL, 0));
	CHECK(gnutls_privkey_import_x509(privkey, private_key, 0));

	CHECK(gnutls_pubkey_init(&pubkey));

	CHECK(gnutls_pubkey_import_privkey(pubkey, privkey, 0, 0));

	CHECK(gnutls_pubkey_get_preferred_hash_algorithm(pubkey, &digest, NULL));

	CHECK(gnutls_privkey_sign_data(privkey, digest, 0, &raw_data,
				       &signature));

	CHECK(gnutls_pubkey_verify_data2(
		pubkey,
		gnutls_pk_to_sign(gnutls_pubkey_get_pk_algorithm(pubkey, NULL),
				  digest),
		vflags, &raw_data, &signature));

	gnutls_pubkey_deinit(pubkey);
	gnutls_privkey_deinit(privkey);
	gnutls_free(signature.data);

    return SUCCESS;

error:
	printf("Error: %s\n", gnutls_strerror_name(ret));
    return ret;
}
