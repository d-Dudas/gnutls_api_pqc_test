# Text colors
# BLACK=\033[0;30m
RED=\033[0;31m
GREEN=\033[0;32m
# YELLOW=\033[0;33m
# BLUE=\033[0;34m
# MAGENTA=\033[0;35m
CYAN=\033[0;36m
# WHITE='033[0;37m
BOLD_WHITE=\033[1;37m
RESET=\033[0m

SHELL := /bin/bash
ROOT_DIR := ./
INCLUDE_DIR := ./include
SRC_DIR := ./src
BUILD_DIR := ./build

CC = gcc
CFLAGS = -Wall -Werror

LIBS = -L${GNUTLS_INSTALL}/lib
INCLUDES = -I${GNUTLS_INSTALL}/include -I$(INCLUDE_DIR)
OTHER_FLAGS = -lgnutls

C_FILES = $(SRC_DIR)/main.c \
		  $(SRC_DIR)/common.c \
		  $(SRC_DIR)/key_generation.c \
		  $(SRC_DIR)/import_export.c \
		  $(SRC_DIR)/sign.c \
		  $(SRC_DIR)/crq.c \
		  $(SRC_DIR)/self_signed_crt.c \
		  $(SRC_DIR)/crt.c \
		  ${SRC_DIR}/crl.c

LOG_FILE = ./logs/test_log_$$(date '+%Y%m%d%H%M%S')

$(shell mkdir -p $(BUILD_DIR))
$(shell mkdir -p ./logs)

all: build test

build: $(BUILD_DIR)/tests

$(BUILD_DIR)/tests:
	@echo -e "$(CYAN)Building...$(RESET)"
	@$(CC) -o $(BUILD_DIR)/tests $(C_FILES) $(INCLUDES) $(LIBS) $(OTHER_FLAGS) $(CFLAGS)
	@echo -e "$(GREEN)[*] Done.$(RESET)"

test:
	@echo -e "$(CYAN)Running tests...$(RESET)"
	@$(BUILD_DIR)/tests #> $(LOG_FILE) # Remove '#' to save logs to file
	@echo -e "$(GREEN)[*] Done.$(RESET)"

clean:
	@echo -e "$(CYAN)Cleaning build directory...$(RESET)"
	@$(RM) build/*
	@echo -e "$(GREEN)[*] Done.$(RESET)"

delete-logs:
	@echo -e "$(CYAN)Deleting logs...$(RESET)"
	@$(RM) logs/*
	@echo -e "$(GREEN)[*] Done.$(RESET)"

.PHONY: all build test clean $(BUILD_DIR)/tests delete-logs
