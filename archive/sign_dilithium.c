/**
 * This program should make a digital signature
 * using dilithium.
 * 
 * It seems that for verification a certificate is 
 * necessary, but this is the scope of another test
 * so this will be left partially incomplet.e
 **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#include "../include/common.h"

int main(int argc, char** argv)
{
    int ret;
    gnutls_x509_privkey_t private_key;
    gnutls_datum_t data;
    unsigned int bits;

    char* buffer = malloc(5 * 1024 * sizeof(char));
    size_t buffer_size = 5 * 1024 * sizeof(char);

    CHECK(gnutls_global_init());
    CHECK(gnutls_x509_privkey_init(&private_key));

    bits = gnutls_sec_param_to_pk_bits(GNUTLS_PK_DILITHIUM3,
					   GNUTLS_SEC_PARAM_ULTRA);
    CHECK(gnutls_x509_privkey_generate(private_key, GNUTLS_PK_DILITHIUM3, bits, 0));

    CHECK(gnutls_load_file("resources/in_data.txt", &data));

    printf("Loaded data: %s\nSize: %lu\n", data.data, data.size);

    CHECK(gnutls_x509_privkey_sign_data(private_key, GNUTLS_DIG_SHAKE_256, 0, &data, buffer, &buffer_size));

    printf("Signed data: %s\nSize: %lu\n", buffer, buffer_size);

    gnutls_x509_privkey_deinit(private_key);
    gnutls_global_deinit();

    return SUCCESS;
error:
    printf("Error: %s\n", gnutls_strerror_name(ret));
    return ERROR;
}
