#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>

#define KEY_LENGTH 512
#define IV_LENGTH 128

int main(int argc, char *argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <encrypt/decrypt> <input_file> <output_file>\n", argv[0]);
        return 1;
    }

    int ret;

    const char *mode = argv[1];
    const char *input_filename = argv[2];
    const char *output_filename = argv[3];

    gnutls_global_init();

    gnutls_cipher_hd_t handle;
    gnutls_cipher_algorithm_t cipher_alg = GNUTLS_CIPHER_KYBER512;
    const gnutls_datum_t key = { (void*)"ThisIsAStrongAESKey1234567890", 800 };
    const gnutls_datum_t iv = { (void*)"RandomInitializationVector", 128 };
    
    // Initialize the cipher handle
    ret = gnutls_cipher_init(&handle, cipher_alg, &key, NULL);
    if (ret < 0) {
        printf("%d\n", ret);
        fprintf(stderr, "Error initializing cipher: %s\n", gnutls_strerror(ret));
        return 1;
    }
    printf("DDUDAS cipher init ok\n");
    // return 0;

    // Open the input and output files
    FILE *input_file = fopen(input_filename, "rb");
    FILE *output_file = fopen(output_filename, "wb");

    if (!input_file || !output_file) {
        perror("Error opening files");
        return 1;
    }

    // Read and encrypt the input file
    unsigned char plaintext[4096];
    unsigned char ciphertext[4096];
    size_t read_bytes;
    int bytes_written;
    while ((read_bytes = fread(plaintext, 1, sizeof(plaintext), input_file)) > 0) {
        printf("DDUDAS IN while\n");
        if (strcmp(mode, "encrypt") == 0) {
            printf("DDUDAS in first if in while\n");
            ret = gnutls_cipher_encrypt2(handle, plaintext, sizeof(plaintext), ciphertext, sizeof(ciphertext));
            if (ret < 0) {
                fprintf(stderr, "Error encrypting data: %s\n", gnutls_strerror(ret));
                return 1;
            }
            printf("DDUDAS after encrypt in while\n");
        } else if (strcmp(mode, "decrypt") == 0) {
            ret = gnutls_cipher_decrypt2(handle, plaintext, read_bytes, ciphertext, sizeof(ciphertext));
            if (ret < 0) {
                fprintf(stderr, "Error decrypting data: %s\n", gnutls_strerror(ret));
                return 1;
            }
        } else {
            printf("Invalid mode\n");
            return 1;
        }
        printf("DDUDAS still in while\n");

        bytes_written = fwrite(ciphertext, 1, read_bytes, output_file);
        if (bytes_written < 0) {
            perror("Error writing to output file");
            return 1;
        }
        printf("Getting out of while\n");
    }
    printf("DDUDAS while done\n");

    // Clean up
    fclose(input_file);
    fclose(output_file);
    printf("DDUDAS FILES CLOSED\n");

    // gnutls_cipher_deinit(handle);
    printf("DDUDAS cipher deinit done\n");
    // gnutls_deinit(session);
    gnutls_global_deinit();
    printf("DDUDAS global deinit done\n");

    return 0;
}
