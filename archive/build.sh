#!/bin/bash

# Text colors
# BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
# YELLOW='\033[0;33m'
# BLUE='\033[0;34m'
# MAGENTA='\033[0;35m'
# CYAN='\033[0;36m'
# WHITE='\033[0;37m'
BOLD_WHITE='\033[1;37m'
RESET='\033[0m'

GNUTLS_INCLUDE='/home/ddudas/gnutls/include'
GNUTLS_LIB='/home/ddudas/gnutls/lib'

INCLUDES="-I${GNUTLS_INCLUDE}"
LIBS="-L${GNUTLS_LIB}"
FLAGS='-lgnutls'

BUILD_DIR='build/'
SRC=
DEST=

function prepare_build_directory()
{
    if [ ! -d "${BUILD_DIR}" ]; then
        echo "Log directory does not exists.Creating it..."
        mkdir -p "${BUILD_DIR}"
    fi
}

function prepare_build_file()
{
    SRC=$1
    if [ ! -e "$SRC" ] || [[ ! "$SRC" == *.c ]]; then
        echo -e "The file ${BOLD_WHITE}$SRC${RESET} either does not exist or does not have a .c extension."
        exit 0
    fi

    DEST=$(basename "$1")
    DEST="${BUILD_DIR}${DEST%.c}"
}

function print_elements()
{
    echo -e "${BOLD_WHITE}Building${RESET} '${SRC}'"
    echo -e "${BOLD_WHITE}Include:${RESET} ${INCLUDES}"
    echo -e "${BOLD_WHITE}Lib:${RESET} ${LIBS}"
    echo -e "${BOLD_WHITE}Flags:${RESET} ${FLAGS}" 
}

function build()
{
    gcc -o "${DEST}" "${SRC}" "${INCLUDES}" "${LIBS}" "${FLAGS}" && \
    echo -e "${GREEN}Build done.${RESET}" && \
    echo -e "${BOLD_WHITE}Executable:${RESET} ${DEST}" && \
    exit 0

    echo -e "${RED}Something went wrong${RESET}"
    exit 0
}

function main()
{
    if [[ $# -ne 1 ]]; then
    {
        echo -e "${BOLD_WHITE}Usage${RESET}: $(basename "$0") <path_to_file>"
        exit 0
    }
    fi
    
    prepare_build_directory
    prepare_build_file "$@"
    print_elements
    build
}

main "$@"
