#include <stdio.h>

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#define ERROR -1

int main(int argc, char** argv)
{
    int ret;
    gnutls_x509_crt_t crt;
    gnutls_certificate_credentials_t credentials;

    ret = gnutls_global_init();
    if(ret < 0) goto error;

    printf("%s: %d\n", __FILE__, __LINE__);
    ret = gnutls_certificate_allocate_credentials(&credentials);
    if(ret < 0) goto error;

    printf("%s: %d\n", __FILE__, __LINE__);
    ret = gnutls_certificate_set_x509_trust_file(credentials, "resources/dilithium_certificate.crt", GNUTLS_X509_FMT_PEM);
    if(ret < 0) goto error;

    // gnutls_certificate_set_x509_crl_file(credentials, "resources/dilithium_certificate.crt", GNUTLS_X509_FMT_PEM);
    printf("%s: %d\n", __FILE__, __LINE__);
    ret = gnutls_certificate_set_x509_key_file(credentials, "resources/dilithium_certificate.crt", "resources/dilithium_private_key.pem", GNUTLS_X509_FMT_PEM);
    if(ret < 0) goto error;

    printf("%s: %d\n", __FILE__, __LINE__);
    gnutls_certificate_free_credentials(credentials);
    gnutls_global_deinit();
    
    return 0;

error:
    gnutls_global_deinit();
    fprintf(stderr, "\033[0;31mError\033[0m: %s\n", gnutls_strerror(ret));
    return ERROR;
}