#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#define BUFFER_SIZE 2048
#define SIMPLE_ERROR -1

int read_certificate(gnutls_datum_t* certificate_data, char* path)
{
    FILE *file = fopen(path, "rb");
    if (!file) {
        perror("Failed to open certificate file");
        return SIMPLE_ERROR;
    }

    fseek(file, 0, SEEK_END);
    size_t file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    certificate_data->data = malloc(file_size);
    if (!certificate_data->data) {
        fclose(file);
        perror("Failed to allocate memory for certificate data");
        return SIMPLE_ERROR;
    }

    certificate_data->size = file_size;

    size_t bytes_read = fread(certificate_data->data, 1, file_size, file);
    if (bytes_read != file_size) {
        fclose(file);
        free(certificate_data->data);
        perror("Failed to read certificate data from file");
        return SIMPLE_ERROR;
    }

    fclose(file);

    return 0;
}

int main(int argc, char *argv[]) {

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <cert_file>\n", argv[0]);
        return SIMPLE_ERROR;
    }

    int ret;
    char *path = argv[1];

    gnutls_x509_crt_t my_certificate;
    gnutls_datum_t certificate_data;

    gnutls_global_init();
    gnutls_x509_crt_init(&my_certificate);

    ret = read_certificate(&certificate_data, path);
    if(ret < 0)
    {
        perror("Failed to read certificate");
        return SIMPLE_ERROR;
    }

    if (gnutls_x509_crt_import(my_certificate, &certificate_data, GNUTLS_X509_FMT_PEM) != GNUTLS_E_SUCCESS) {
        fprintf(stderr, "Failed to import certificate\n");
        return 1;
    }

    char *subject = malloc(BUFFER_SIZE * sizeof(char));
    char *issuer = malloc(BUFFER_SIZE * sizeof(char));
    size_t subject_size = BUFFER_SIZE;
    size_t issuer_size = BUFFER_SIZE;

    gnutls_x509_crt_get_dn(my_certificate, subject, &subject_size);
    gnutls_x509_crt_get_issuer_dn(my_certificate, issuer, &issuer_size);
    time_t expiration = gnutls_x509_crt_get_expiration_time(my_certificate);
    time_t activation = gnutls_x509_crt_get_activation_time(my_certificate);

    printf("Subject: %s\nIssuer: %s\nExpiration: %d\nActivation: %d\n", subject, issuer, expiration, activation);

    gnutls_x509_crt_deinit(my_certificate);
    gnutls_global_deinit();

    return 0;
}