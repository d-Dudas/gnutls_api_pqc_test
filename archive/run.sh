#!/bin/bash

# Text colors
# BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
# BLUE='\033[0;34m'
# MAGENTA='\033[0;35m'
# CYAN='\033[0;36m'
# WHITE='\033[0;37m'
BOLD_WHITE='\033[1;37m'
RESET='\033[0m'

LOG_FILE='1'
RUN_FILE=
KEEP_LOG=false

function parse_options()
{
    while [[ $# -gt 0 ]]; do
    {
        case "$1" in
            --keep-log)
                KEEP_LOG=true
                ;;
            *)
                RUN_FILE="$1"
                ;;
        esac
        shift
    }
    done
}

function prepare_log_file()
{
    program_name=$(basename "$1")
    timestamp=$(date '+%Y%m%d%H%M%S')
    log_dir="logs/"
    log_file="${log_dir}${program_name}_${timestamp}.log"
    if [ ! -d "${log_dir}" ]; then
        echo "Log directory does not exists.Creating it..."
        mkdir -p "${log_dir}"
    fi
    LOG_FILE="${log_file}"
}

# Prints how to use the script 
function print_usage()
{
    echo -e "$(basename "$0") <path_to_file> [--keep-log]"
}

# Prints a general error message
function err()
{
    echo -e "${RED}Something went wrong.${RESET}"
    echo -e "Logs in: ${YELLOW}${LOG_FILE}${RESET}"
    exit 1
}

# Prints a general success message
function success()
{
    echo -e "${GREEN}[*] Done.${RESET}"

    if [ "${KEEP_LOG}" == false ]; then
    {
        rm "${LOG_FILE}"
    } else
    {
        echo -e "Logs in: ${YELLOW}${LOG_FILE}${RESET}"
    }
    fi

    exit 0
}

# Run the test_file_encrypt
# Hardcoded because of the arguments
function run_enc()
{
    echo -e "Encrypting file ${BOLD_WHITE}'file_to_encrypt.txt'${RESET} to ${BOLD_WHITE}'encrypted_file.txt'${RESET}" && \
    ./build/test_file_encrypt encrypt ./resources/file_to_encrypt.txt ./resources/encrypted_file.txt && \
    echo -e "Decrypting file ${BOLD_WHITE}'encrypted_file.txt'${RESET} to ${BOLD_WHITE}'decrypted_file.txt'${RESET}" && \
    ./build/test_file_encrypt decrypt ./resources/encrypted_file.txt ./resources/decrypted_file.txt && \
    success

    err
}

# Run the test_x509
# Hardcoded because of arguments
function run_cert()
{
    echo -e "Using certificate ${BOLD_WHITE}'resources/example.crt'${RESET}" && \
    ./build/test_x509 resources/example.crt && \
    success

    err
}

# Main function
function main()
{
    if [[ $# -gt 2 || $# -lt 1 ]]; then
    {
        print_usage
        exit 0
    }
    fi

    parse_options "$@"

    if [[ ! -e "${RUN_FILE}" ]]; then
    {
        echo -e "$(basename "$0"): ${YELLOW}File not found${RESET}: ${RUN_FILE}"
        exit 1
    }
    fi

    prepare_log_file "$@"

    case "${RUN_FILE}" in
        build/test_file_encrypt | ./build/test_file_encrypt)
            run_enc
            ;;
        build/test_x509 | ./build/test_x509)
            run_cert
            ;;
        *)
            $1 > "${LOG_FILE}" && success
            err
            ;;
    esac
}

main "$@"
