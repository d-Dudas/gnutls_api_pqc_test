/* This example code is placed in the public domain. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <time.h>

/* This example will generate a private key and a certificate
 * request.
 */

#define ERROR -1

int save_to_file(char* filename, char** buffer, size_t buffer_size)
{
	FILE* output_file = fopen(filename, "wb");
	if(output_file == NULL) return ERROR;

	size_t elements_written = fwrite(buffer, buffer_size, 1, output_file);
	
	fclose(output_file);

	if(elements_written != buffer_size) return ERROR;

	return 0;
}

int main(int argc, char** argv)
{
	gnutls_x509_crq_t crq;
	gnutls_x509_privkey_t key;
	unsigned char buffer[10 * 1024];
	size_t buffer_size = sizeof(buffer);
	unsigned int bits;

    int ret;

	ret = gnutls_global_init();
	if (ret < 0) goto error;

	/* Initialize an empty certificate request, and
	 * an empty private key.
	 */
	ret = gnutls_x509_crq_init(&crq);
	if (ret < 0) goto error;

	ret = gnutls_x509_privkey_init(&key);
	if (ret < 0) goto error;

	/* Generate an RSA key of moderate security.
	 */
	bits = gnutls_sec_param_to_pk_bits(GNUTLS_PK_DILITHIUM3,
					   GNUTLS_SEC_PARAM_MEDIUM);
	ret = gnutls_x509_privkey_generate(key, GNUTLS_PK_DILITHIUM3, bits, 0);
	if (ret < 0) goto error;

	/* Add stuff to the distinguished name
	 */
	ret = gnutls_x509_crq_set_dn_by_oid(crq, GNUTLS_OID_X520_COUNTRY_NAME, 0,
				      "GR", 2);
	if (ret < 0) goto error;

	ret = gnutls_x509_crq_set_dn_by_oid(crq, GNUTLS_OID_X520_COMMON_NAME, 0,
				      "Nikos", strlen("Nikos"));
	if (ret < 0) goto error;

	/* Set the request version.
	 */
	ret = gnutls_x509_crq_set_version(crq, 1);
	if (ret < 0) goto error;

	/* Set a challenge password.
	 */
	ret = gnutls_x509_crq_set_challenge_password(crq,
					       "something to remember here");
	if (ret < 0) goto error;

	/* Associate the request with the private key
	 */
	ret = gnutls_x509_crq_set_key(crq, key);
	if (ret < 0) goto error;

	/* Self sign the certificate request.
	 */
	ret = gnutls_x509_crq_sign2(crq, key, GNUTLS_DIG_SHAKE_256, 0);
	if (ret < 0) goto error;

	/* Export the PEM encoded certificate request, and
	 * display it.
	 */
	ret = gnutls_x509_crq_export(crq, GNUTLS_X509_FMT_PEM, buffer, &buffer_size);
	if (ret < 0) goto error;

	printf("Certificate Request: \n%s", buffer);
	// ret = save_to_file("resources/dilithium_certificate.crt", buffer, buffer_size);
	if (ret < 0) goto error;

	char *subject = malloc(1024 * sizeof(char));
    size_t subject_size = 1024;

	gnutls_x509_crq_get_dn(crq, subject, &subject_size);

    printf("Subject: %s\n", subject);

	/* Export the PEM encoded private key, and
	 * display it.
	 */
	buffer_size = sizeof(buffer);
	ret = gnutls_x509_privkey_export(key, GNUTLS_X509_FMT_PEM, buffer,
				   &buffer_size);
	if (ret < 0) goto error;

	printf("\n\nPrivate key: \n%s", buffer);
	// ret = save_to_file("resources/dilithium_private_key.key", buffer, buffer_size);
	if (ret < 0) goto error;

	gnutls_x509_crq_deinit(crq);
	gnutls_x509_privkey_deinit(key);

	return 0;

error:
    gnutls_global_deinit();
    fprintf(stderr, "\033[0;31mError\033[0m: %s\n", gnutls_strerror(ret));
    return ERROR;
}
