#include <stdio.h>
#include <stdlib.h>

#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#include <gnutls/x509.h>

#define ERROR -1

int main(int argc, char** args)
{
    int ret;
    gnutls_x509_privkey_t private_key;
    gnutls_datum_t data;
    unsigned int bits;

    ret = gnutls_global_init();
    if (ret < 0) goto error;

    ret = gnutls_x509_privkey_init(&private_key);
    if (ret < 0) goto error;

    bits = gnutls_sec_param_to_pk_bits(GNUTLS_PK_DILITHIUM3,
					   GNUTLS_SEC_PARAM_ULTRA);
    ret = gnutls_x509_privkey_generate(private_key, GNUTLS_PK_DILITHIUM3, bits, 0);
    if (ret < 0) goto error;

    // Export private key to data
    data.data = malloc(bits * sizeof(char));
    data.size = bits;

    ret = gnutls_x509_privkey_export2_pkcs8(private_key, GNUTLS_X509_FMT_PEM, NULL, 0, &data);
    if(ret < 0) goto error;

    printf("%s", data.data);

    gnutls_x509_privkey_deinit(private_key);
    gnutls_global_deinit();

    return 0;

error:
    gnutls_global_deinit();
    fprintf(stderr, "\033[0;31mError\033[0m: %s\n", gnutls_strerror(ret));
    return ERROR;
}