#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#define ERROR(line) {printf("Line: %d\n", line); goto error;}

void error(int ret, int line)
{
    printf("line: %d\n", line);
    gnutls_strerror_name(ret);
    exit(-1);
}

int generate_private_key(gnutls_x509_privkey_t *key)
{
    unsigned int bits;
    int ret;

    ret = gnutls_x509_privkey_init(key);
    if(ret < 0) error(ret, __LINE__);

    bits = gnutls_sec_param_to_pk_bits(GNUTLS_PK_RSA,
                    GNUTLS_SEC_PARAM_MEDIUM);
    ret = gnutls_x509_privkey_generate(*key, GNUTLS_PK_DILITHIUM3, bits, 0);
    if(ret < 0) error(ret, __LINE__);

    return 0;
}

int generate_certificate_request(gnutls_x509_crq_t *crq, gnutls_x509_privkey_t *key)
{
    int ret;

    ret = gnutls_x509_crq_init(crq);
    if(ret < 0) error(ret, __LINE__);

    ret = gnutls_x509_crq_set_dn_by_oid(*crq, GNUTLS_OID_X520_COUNTRY_NAME, 0,
				      "GR", 2);
    if(ret < 0) error(ret, __LINE__);

    ret = gnutls_x509_crq_set_version(*crq, 1);
    if(ret < 0) error(ret, __LINE__);

    ret = gnutls_x509_crq_set_challenge_password(*crq,
					       "something to remember here");
    if(ret < 0) error(ret, __LINE__);

    ret = gnutls_x509_crq_set_key(*crq, *key);
	if(ret < 0) error(ret, __LINE__);

    ret = gnutls_x509_crq_sign2(*crq, *key, GNUTLS_DIG_SHAKE_256, 0);
    if(ret < 0) error(ret, __LINE__);

    return 0;
}

int main()
{
    int ret;
    gnutls_x509_privkey_t key;
    gnutls_x509_crq_t crq;
    gnutls_x509_crt_t cert;
    gnutls_datum_t out;

    ret = gnutls_global_init();
    if(ret < 0) ERROR(__LINE__);

    generate_private_key(&key);
    generate_certificate_request(&crq, &key);

    // initialize the certificate and add the key and crq
    ret = gnutls_x509_crt_init(&cert);
    if(ret < 0) ERROR(__LINE__);
    ret = gnutls_x509_crt_set_key(cert, key);
    if(ret < 0) ERROR(__LINE__);
    ret = gnutls_x509_crt_set_crq(cert, crq);
    if(ret < 0) ERROR(__LINE__);

    // set dn
    ret = gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_COMMON_NAME, 0, "DDUDAS", strlen("DDUDAS"));
    if(ret < 0) ERROR(__LINE__);

    // set activation and expiration time
    ret = gnutls_x509_crt_set_activation_time(cert, time(NULL));
    if(ret < 0) ERROR(__LINE__);
    ret = gnutls_x509_crt_set_expiration_time(cert, time(NULL) + 365 * 24 * 60 * 60); // 1 year validity
    if(ret < 0) ERROR(__LINE__);

    // export it
    ret = gnutls_x509_crt_export2(cert, GNUTLS_X509_FMT_PEM, &out);
    if(ret < 0) ERROR(__LINE__);
    printf("%s\n", out.data);

    gnutls_x509_crt_deinit(cert);
    gnutls_x509_crq_deinit(crq);
    gnutls_x509_privkey_deinit(key);
    gnutls_global_deinit();

    return 0;

error:
    printf("Something went wrong: %s\n", gnutls_strerror_name(ret));
    return -1;
}

/* ARCHIVE

gnutls_x509_crt_set_version(cert, 3));
gnutls_x509_crt_set_serial(cert, 1));
gnutls_x509_crt_set_dn_by_oid(cert, GNUTLS_OID_X520_COUNTRY_NAME, 0,
				       "GR", 2));
gnutls_x509_crt_set_issuer_dn_by_oid(cert, GNUTLS_OID_X520_COMMON_NAME, 0,
				       "Nikos", strlen("Nikos")));
gnutls_x509_crt_set_key_usage(cert, 0x80)); // Digital Signature
gnutls_x509_crt_set_key_purpose(cert, 1));   // SSL/TLS Server Authentication
gnutls_x509_crt_sign(cert, cert, key));

*/