#pragma once

#define PKCS1_FILE2 "resources/key2.pem"
#define PKCS8_FILE2 "resources/key2_pkcs8.pem"

#define CRT_SELF_SIGNED_FILE2 "resources/self-signed-certificate2.crt"

#define CRQ_FILE2 "resources/certificate_request2.pem"

#define CRT_FILE2 "resources/certificate2.crt"

int test_key_import_export();
int verify_pkcs1_import_export();
int verify_pkcs8_import_export();

int verify_crq_import_export();
int test_crq_import_export();

int verify_sscrt_import_export();
int test_sscrt_import_export();

int verify_crt_import_export();
int test_crt_import_export();
