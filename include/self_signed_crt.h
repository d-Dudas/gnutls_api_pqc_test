#pragma once

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#define CERT_COUNTRY_NAME "RO"
#define CERT_ORG_NAME "Best Organization"
#define CERT_ORG_UNIT_NAME "Best Organization SRL"
#define CERT_SERIAL "0987654321"
#define CERT_SUBJECT_NAME "Luis"
#define CERT_ISSUER_NAME "Elefant"

int sscrt_set_key_and_crq(gnutls_x509_crt_t crt, gnutls_x509_privkey_t pkey);
int sscrt_set_tbs_params(gnutls_x509_crt_t crt);
int test_self_signed_crt();
