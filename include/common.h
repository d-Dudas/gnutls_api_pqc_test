#pragma once

#include <gnutls/gnutls.h>

#define SUCCESS 0
#define ERROR -1
#define TEST_FAILED -2

#define PKCS1_FILE "resources/pkey.pem"
#define PKCS8_FILE "resources/pkey_pkcs8.pem"
#define CRQ_FILE "resources/certificate_request.pem"
#define CRT_SELF_SIGNED_FILE "resources/self-signed-certificate.crt"
#define CRT_FILE "resources/certificate.crt"
#define CRL_FILE "resources/crl.pem"
#define SELF_SIGNED_CRT_SUBJECT_ID "87529564739abc2"

#define CHECK(result) {ret = result; if(ret < 0) {printf("Failure on: %s:%d\n", __FILE__, __LINE__); goto error;}}
#define TEST(result) {ret = result; if(ret < 0) goto test_failed;}
#define TEST_MSG(result, msg) {ret = result; if(ret < 0) { printf("%s\n", msg); goto test_failed; }}

int save_to_file2(char* filename, char* buffer, size_t buffer_size);
int save_to_file(char* filename, unsigned char* uBuffer, size_t buffer_size);
int cmp_files(char* first_file, char* second_file);
gnutls_digest_algorithm_t get_pk_digest(gnutls_pk_algorithm_t algo);
