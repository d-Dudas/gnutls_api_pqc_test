#pragma once

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

#define CERT_COUNTRY_NAME "RO"
#define CERT_ORG_NAME "Best Organization"
#define CERT_ORG_UNIT_NAME "Best Organization SRL"
#define CERT_SERIAL "0987654321"
#define CERT_SUBJECT_NAME "Luis"
#define CERT_ISSUER_NAME "Elefant"
#define SUBJECT_ID "87529564739abc2ASDX"

int crt_set_key_and_crq(gnutls_x509_crt_t crt, gnutls_x509_privkey_t pkey);
int crt_set_tbs_params(gnutls_x509_crt_t crt);
int import_self_signed_crt(gnutls_x509_crt_t self_signed_crt);
int test_crt();
