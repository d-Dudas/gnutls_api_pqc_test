#pragma once

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>

int test_sign_data(gnutls_pk_algorithm_t algorithm);
